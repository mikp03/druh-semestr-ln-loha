package logika;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Objednavka implements Serializable {

	private SimpleIntegerProperty idObjednavky1;
	private SimpleStringProperty jmenoZamestnanceHlidani1;
	private SimpleStringProperty poznamka1;
	private SimpleStringProperty datum1;
	private SimpleStringProperty casZacatek1;
	private SimpleStringProperty mistoZacatek1;
	private SimpleIntegerProperty cena1;
	private SimpleBooleanProperty stav1;

	public Objednavka(int idObjednavky, String jmenoZamestnanceHlidani, String datum, String casZacatek, String mistoZacatek, int cena, String poznamka, boolean stav) {
		this.idObjednavky1 = new SimpleIntegerProperty(idObjednavky);
		this.jmenoZamestnanceHlidani1 = new SimpleStringProperty(jmenoZamestnanceHlidani);
		this.datum1 = new SimpleStringProperty(datum);
		this.casZacatek1 = new SimpleStringProperty(casZacatek);
		this.mistoZacatek1 = new SimpleStringProperty(mistoZacatek);
		this.cena1 = new SimpleIntegerProperty(cena);
		this.poznamka1 = new SimpleStringProperty(poznamka);
		this.stav1 = new SimpleBooleanProperty(stav);
	}

	public String getPoznamka() {
		return poznamka1.get();
	}

	public void setPoznamka(String poznamka) {
		this.poznamka1.set(poznamka);
	}

	public int getIdObjednavky() {
		return idObjednavky1.get();
	}

	public void setIdObjednavky(int idObjednavky) {
		this.idObjednavky1.set(idObjednavky);
	}

	public String getJmenoZamestnanceHlidani() {
		return jmenoZamestnanceHlidani1.get();
	}

	public void setJmenoZamestnanceHlidani(String jmenoZamestnanceHlidani) {
		this.jmenoZamestnanceHlidani1.set(jmenoZamestnanceHlidani);
	}

	public int getCena() {
		return cena1.get();
	}

	public void setCena(int cena) {
		this.cena1.set(cena);
	}

	public String getMistoZacatek() {
		return mistoZacatek1.get();
	}

	public void setMistoZacatek(String mistoZacatek) {
		this.mistoZacatek1.set(mistoZacatek);
	}

	public String getCasZacatek() {
		return casZacatek1.get();
	}

	public void setCasZacatek(String casZacatek) {
		this.casZacatek1.set(casZacatek);
	}

	public String getDatum() {
		return datum1.get();
	}

	public void setDatum(String datum) {
		this.datum1.set(datum);
	}

	public boolean getStav() {
		return stav1.get();
	}

	public void setStav(boolean stav) {
		this.stav1.set(stav);
	}

	//int idObjednavky, String jmenoZamestnanceHlidani, String datum, String casZacatek, String mistoZacatek, int cena, String poznamka, boolean stav

	private void readObject(ObjectInputStream in)
			throws IOException, ClassNotFoundException {
		idObjednavky1 = new SimpleIntegerProperty((int) in.readObject());
		jmenoZamestnanceHlidani1 = new SimpleStringProperty((String) in.readObject());
		datum1 = new SimpleStringProperty((String) in.readObject());
		casZacatek1 = new SimpleStringProperty((String) in.readObject());
		mistoZacatek1 = new SimpleStringProperty((String) in.readObject());
		cena1 = new SimpleIntegerProperty((int) in.readObject());
		poznamka1 = new SimpleStringProperty((String) in.readObject());
		stav1 = new SimpleBooleanProperty((boolean) in.readObject());
	}

	private void writeObject(ObjectOutputStream out)
			throws IOException {
		out.writeObject(getIdObjednavky());
		out.writeObject(getJmenoZamestnanceHlidani());
		out.writeObject(getDatum());
		out.writeObject(getCasZacatek());
		out.writeObject(getMistoZacatek());
		out.writeObject(getCena());
		out.writeObject(getPoznamka());
		out.writeObject(getStav());
	}
}
