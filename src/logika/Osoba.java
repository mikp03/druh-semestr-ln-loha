package logika;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Osoba implements Serializable {

	private SimpleStringProperty jmenoOsoby;

	private SimpleStringProperty prijmeniOsoby;

	private SimpleIntegerProperty rokNarozeniOsoby;

	private SimpleStringProperty emailOsoby;

	private SimpleIntegerProperty telefonOsoby;

	public Osoba(String jmeno, String prijmeni, int rokNarozeni, String email) {
		this.jmenoOsoby = new SimpleStringProperty(jmeno);
		this.prijmeniOsoby = new SimpleStringProperty(prijmeni);
		this.rokNarozeniOsoby = new SimpleIntegerProperty(rokNarozeni);
		this.emailOsoby = new SimpleStringProperty(email);
	}

	public void setTelefon(int telefon) {
		telefonOsoby.set(telefon);
	}

	public int getTelefon(){
		return telefonOsoby.get();
	}

	public String getJmeno() {
		return jmenoOsoby.get();
	}

	public void setJmeno(String jmeno) {
		jmenoOsoby.set(jmeno);
	}

	public String getPrijmeni() {
		return prijmeniOsoby.get();
	}

	public void setPrijmeni(String prijmeni) {
		prijmeniOsoby.set(prijmeni);
	}

	public String getCeleJmeno () { return jmenoOsoby + " "+ prijmeniOsoby;}

	public String getEmail() {
		return emailOsoby.get();
	}

	public void setEmail(String email) {
		emailOsoby.set(email);
	}

	private void readObject(ObjectInputStream in)
			throws IOException, ClassNotFoundException {
		jmenoOsoby = new SimpleStringProperty((String) in.readObject());
		prijmeniOsoby = new SimpleStringProperty((String) in.readObject());
		rokNarozeniOsoby = new SimpleIntegerProperty((int) in.readObject());
		emailOsoby = new SimpleStringProperty((String) in.readObject());
	}

	private void writeObject(ObjectOutputStream out)
			throws IOException {
		out.writeObject(getJmeno());
		out.writeObject(getPrijmeni());
		out.writeObject(getRokNarozeni());
		out.writeObject(getEmail());
	}

	public int getRokNarozeni() {
		return rokNarozeniOsoby.get();
	}

	public SimpleIntegerProperty rokNarozeniOsobyProperty() {
		return rokNarozeniOsoby;
	}

	public void setRokNarozeniOsoby(int rokNarozeni) {
		this.rokNarozeniOsoby.set(rokNarozeni);
	}
}
