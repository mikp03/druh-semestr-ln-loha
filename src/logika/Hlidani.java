package logika;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Hlidani implements Serializable {

	private SimpleStringProperty typHlidaniSluzby;

	private SimpleStringProperty delkaHlidaniSluzby;

	private SimpleStringProperty popisHlidaniSluzby;

	public Hlidani(String typHlidani, String delkaHlidani, String popisHlidani) {
		this.typHlidaniSluzby = new SimpleStringProperty(typHlidani);
		this.delkaHlidaniSluzby = new SimpleStringProperty(delkaHlidani);
		this.popisHlidaniSluzby = new SimpleStringProperty(popisHlidani);
	}

	public String getDelkaHlidani() {
		return delkaHlidaniSluzby.get();
	}

	public void setDelkaHlidani(String delkaHlidani) {
		delkaHlidaniSluzby.set(delkaHlidani);
	}

	public String getTypHlidani() {
		return typHlidaniSluzby.get();
	}

	public void setTypHlidani(String typHlidani) {
		typHlidaniSluzby.set(typHlidani);
	}

	public String getPopisHlidani() {
		return popisHlidaniSluzby.get();
	}

	public void setPopisHlidani(String popisHlidani) {
		popisHlidaniSluzby.set(popisHlidani);
	}

	private void readObject(ObjectInputStream in)
			throws IOException, ClassNotFoundException {
		typHlidaniSluzby = new SimpleStringProperty((String) in.readObject());
		delkaHlidaniSluzby = new SimpleStringProperty((String) in.readObject());
		popisHlidaniSluzby = new SimpleStringProperty((String) in.readObject());
	}

	private void writeObject(ObjectOutputStream out)
			throws IOException {
		out.writeObject(getTypHlidani());
		out.writeObject(getDelkaHlidani());
		out.writeObject(getPopisHlidani());
	}
}
