package logika;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class ZamestnanecHlidani extends Osoba implements Serializable {

	private SimpleIntegerProperty maxPocetDeti;

	private SimpleIntegerProperty hodnoceni;

	public ZamestnanecHlidani(String jmeno, String prijmeni, int rokNarozeni, String email, int mPDeti, int hodn) {
		super(jmeno, prijmeni, rokNarozeni, email);
		this.maxPocetDeti = new SimpleIntegerProperty(mPDeti);
		this.hodnoceni = new SimpleIntegerProperty(hodn);	}

	public int getHodnoceni() {
		return hodnoceni.get();
	}

	public void setHodnoceni(int hodn) {
		hodnoceni.set(hodn);
	}

	public int getMaxPocetDeti() {
		return maxPocetDeti.get();
	}

	public void setMaxPocetDeti(int mPDeti) {
		maxPocetDeti.set(mPDeti);
	}

	private void readObject(ObjectInputStream in)
			throws IOException, ClassNotFoundException {
		maxPocetDeti = new SimpleIntegerProperty((int) in.readObject());
		hodnoceni = new SimpleIntegerProperty((int) in.readObject());
	}

	private void writeObject(ObjectOutputStream out)
			throws IOException {
		out.writeObject(getMaxPocetDeti());
		out.writeObject(getHodnoceni());
	}
}
