package logika;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Klient extends Osoba implements Serializable {

	private SimpleIntegerProperty pocetDetiKlienta;

	public Klient(String jmeno, String prijmeni, int rokNarozeni, String email, int pocetDeti) {
		super(jmeno, prijmeni, rokNarozeni, email);
		this.pocetDetiKlienta = new SimpleIntegerProperty(pocetDeti);
	}

	private void readObject(ObjectInputStream in)
			throws IOException, ClassNotFoundException {
		pocetDetiKlienta = new SimpleIntegerProperty((int) in.readObject());
	}

	private void writeObject(ObjectOutputStream out)
			throws IOException {
		out.writeObject(getPocetDeti());
	}

	public int getPocetDeti() {
		return pocetDetiKlienta.get();
	}

	public void setPocetDetiKlienta(int pocetDetiKlienta) {
		this.pocetDetiKlienta.set(pocetDetiKlienta);
	}
}
