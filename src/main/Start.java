package main;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class Start extends Application{

	private MenuBar menuBar;
	private AnchorPane anchorPane;

	public static void main(String[] args) {
		Application.launch(args);
	}

	/**
	 * Metoda, která vykreslí startovní okno aplikace
	 *
	 * @param stage zakládaná scéna
	 * @throws Exception
	 */

	public void start(Stage stage) throws IOException {
/*		Scene scene = new Scene (new Group());
		stage.setTitle("Péťova apka");
		stage.setWidth(450);
		stage.setHeight(500);

		BorderPane borderPane = new BorderPane();
		borderPane.setTop(menuBar);

		Menu menuMenu = new Menu("Menu");
		MenuItem poskytovateleHlidani = new MenuItem("Poskytovatelé hlídání");
		MenuItem sluzby = new MenuItem("Služby");
		menuMenu.getItems().addAll(poskytovateleHlidani,sluzby);

		Menu menuPrihlaseni = new Menu ("Přihlášení");
		MenuItem prihlaseni = new Menu ("Přihlášení");
		menuPrihlaseni.getItems().addAll(prihlaseni);

		borderPane.setCenter(anchorPane);

		Button button1 = new Button("Poskytovatelé hlídání");
		button1.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

			}
		});*/

		Parent root = FXMLLoader.load(getClass().getResource("/fxml/UvodniObrazovka.fxml"));
		Scene scene = new Scene(root);

		stage.setScene(scene);
		stage.setTitle("Péťova hlídací služba");
		stage.show();
	}

	/*public void start (Stage primaryStage){
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				Platform.exit();
				System.exit(0);
			}
		});

		BorderPane borderPane = new BorderPane();
		ToolBar nastrojovaLista = new ToolBar();

		Button button1 = new Button("Přihlášení");
		nastrojovaLista.getItems().add(button1);
		button1.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Dialog<Pair<String, String>> dialog = new Dialog<>();
				dialog.setTitle("Login Dialog");
				dialog.setHeaderText("Look, a Custom Login Dialog");
				ButtonType loginButtonType = new ButtonType("Login", ButtonBar.ButtonData.OK_DONE);
				dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);
				GridPane grid = new GridPane();
				grid.setHgap(10);
				grid.setVgap(10);
				grid.setPadding(new Insets(20, 150, 10, 10));

				TextField username = new TextField();
				username.setPromptText("Username");
				PasswordField password = new PasswordField();
				password.setPromptText("Password");

				grid.add(new Label("Username:"), 0, 0);
				grid.add(username, 1, 0);
				grid.add(new Label("Password:"), 0, 1);
				grid.add(password, 1, 1);

				Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
				loginButton.setDisable(true);
				//validation
				username.textProperty().addListener((observable, oldValue, newValue) -> {
					loginButton.setDisable(newValue.trim().isEmpty());
				});

				dialog.getDialogPane().setContent(grid);
				Platform.runLater(() -> username.requestFocus());
				dialog.setResultConverter(dialogButton -> {
					if (dialogButton == loginButtonType) {
						return new Pair<>(username.getText(), password.getText());
					}
					return null;
				});

				Optional<Pair<String, String>> result = dialog.showAndWait();

				result.ifPresent(usernamePassword -> {
					System.out.println("Username=" + usernamePassword.getKey() + ", Password=" + usernamePassword.getValue());
				});
			}
		});
		borderPane.setLeft(PanelTabulky.getPanelTabulky());
		//boredPane.setRight(PanelHlidani.getPanelHlidani());

	}*/

}
