package ui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import table.PanelTabulkyHlidani;
import table.PanelTabulkyKlientu;

import java.awt.*;
import java.net.URL;
import java.util.ResourceBundle;

public class ZalozeniSluzby implements Initializable {
    @FXML
    private BorderPane borderPane;

    /**
     * Metoda, která po zavolání vykreslí scénu s úvodní stránkou.
     *
     * @param event událost při které se má metoda provést
     * @throws Exception
     */
    @FXML
    public void loadUvodniStranka(ActionEvent event) throws Exception {
        BorderPane pane = FXMLLoader.load(getClass().getResource("/fxml/UvodniObrazovka.fxml"));
        borderPane.getChildren().setAll(pane);
    }

    /**
     * Metoda, která po zavolání vykreslí scénu s výpisem všech poskytovatelů hlídání.
     *
     * @param event událost při které se má metoda provést
     * @throws Exception
     */
    @FXML
    public void loadPoskytovateleHlidani(ActionEvent event) throws Exception {
        BorderPane pane = FXMLLoader.load(getClass().getResource("/fxml/SeznamPoskytovateluHlidani.fxml"));
        borderPane.getChildren().setAll(pane);
    }

    /**
     * Metoda, která po zavolání vykreslí scénu s výpisem všech služeb.
     *
     * @param event událost při které se má metoda provést
     * @throws Exception
     */
    @FXML
    public void loadSluzby(ActionEvent event) throws Exception {
        PanelTabulkyHlidani panelTabulky = new PanelTabulkyHlidani();
        panelTabulky.nactiSeznam();
        BorderPane pane = FXMLLoader.load(getClass().getResource("/fxml/SeznamSluzeb.fxml"));
        borderPane.getChildren().setAll(pane);
    }

    /**
     * Metoda, která po zavolání vykreslí scénu s výpisem všech objednavek.
     *
     * @param event událost při které se má metoda provést
     * @throws Exception
     */
    @FXML
    public void loadObjednavky(ActionEvent event) throws Exception {
        BorderPane pane = FXMLLoader.load(getClass().getResource("/fxml/SeznamObjednavek.fxml"));
        borderPane.getChildren().setAll(pane);
    }

    /**
     * Metoda, která po zavolání vykreslí scénu s výpisem všech klientů.
     *
     * @param event událost při které se má metoda provést
     * @throws Exception
     */
    @FXML
    public void loadKlienti(ActionEvent event) throws Exception {
        BorderPane pane = FXMLLoader.load(getClass().getResource("/fxml/SeznamKlientu.fxml"));
        borderPane.getChildren().setAll(pane);
    }

    /**
     * Metoda voláná před ukládáním dat do databáze. Kontroluje vstupy, zda
     * splňují podmínky pro uložení, zobrazuje modální okna s informacemi o
     * průběhu ukládání.
     *
     * @param event událost při které se má metoda provést
     */
    @FXML
    public void potvrdit(ActionEvent event) {
        PanelTabulkyHlidani panelTabulky = new PanelTabulkyHlidani();
        panelTabulky.ulozSeznam();
    }

    /**
     * Metoda naplňující grafické prvky daty z databáze při vytvoření scény.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

}
