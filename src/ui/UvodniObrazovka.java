package ui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import table.PanelTabulkyHlidani;
import table.PanelTabulkyKlientu;
import table.PanelTabulkyObjednavek;
import table.PanelTabulkyPoskytovateleHlidani;

import java.net.URL;
import java.util.ResourceBundle;

public class UvodniObrazovka implements Initializable {

    @FXML
    private BorderPane borderPane;

    @FXML
    private MenuItem poskytovateleHlidani;

    @FXML
    private Button poskytovateleHlidaniButton;

    @FXML
    private MenuItem sluzby;

    @FXML
    private Button sluzbyButton;

    @FXML
    private Menu menu;

    @FXML
    private Menu prihlaseni;

    @FXML
    private MenuBar menuBar;

    @FXML
    private MenuItem prihlaseniItem;

    /**
     * Metoda, která po zavolání vykreslí scénu s úvodní stránkou.
     *
     * @param event událost při které se má metoda provést
     * @throws Exception
     */
    @FXML
    public void loadUvodniStranka(ActionEvent event) throws Exception {
        BorderPane pane = FXMLLoader.load(getClass().getResource("/fxml/UvodniObrazovka.fxml"));
        borderPane.getChildren().setAll(pane);
    }

    /**
     * Metoda, která po zavolání vykreslí scénu s výpisem všech poskytovatelů hlídání.
     *
     * @param event událost při které se má metoda provést
     * @throws Exception
     */
    @FXML
    public void loadPoskytovateleHlidani(ActionEvent event) throws Exception {
        BorderPane pane = FXMLLoader.load(getClass().getResource("/fxml/SeznamPoskytovateluHlidani.fxml"));
        borderPane.getChildren().setAll(pane);
    }

    /**
     * Metoda, která po zavolání vykreslí scénu s výpisem všech služeb.
     *
     * @param event událost při které se má metoda provést
     * @throws Exception
     */
    @FXML
    public void loadSluzby(ActionEvent event) throws Exception {
        BorderPane pane = FXMLLoader.load(getClass().getResource("/fxml/SeznamSluzeb.fxml"));
        borderPane.getChildren().setAll(pane);
    }

    /**
     * Metoda, která po zavolání vykreslí scénu s výpisem všech objednavek.
     *
     * @param event událost při které se má metoda provést
     * @throws Exception
     */
    @FXML
    public void loadObjednavky(ActionEvent event) throws Exception {
        BorderPane pane = FXMLLoader.load(getClass().getResource("/fxml/SeznamObjednavek.fxml"));
        borderPane.getChildren().setAll(pane);
    }

    /**
     * Metoda, která po zavolání vykreslí scénu s výpisem všech klientů.
     *
     * @param event událost při které se má metoda provést
     * @throws Exception
     */
    @FXML
    public void loadKlienti(ActionEvent event) throws Exception {
        BorderPane pane = FXMLLoader.load(getClass().getResource("/fxml/SeznamKlientu.fxml"));
        borderPane.getChildren().setAll(pane);
    }

    /**
     * Metoda, která po zavolání vykreslí scénu s výpisem všech služeb.
     *
     * @param event událost při které se má metoda provést
     * @throws Exception
     */
    @FXML
    public void loadData(ActionEvent event) throws Exception {
        PanelTabulkyPoskytovateleHlidani panelTabulkyPoskytovateleHlidani = new PanelTabulkyPoskytovateleHlidani();
        panelTabulkyPoskytovateleHlidani.ulozPoprveSeznam();
        PanelTabulkyObjednavek panelTabulkyObjednavek = new PanelTabulkyObjednavek();
        panelTabulkyObjednavek.ulozPoprveSeznam();
        PanelTabulkyKlientu panelTabulkyKlientu = new PanelTabulkyKlientu();
        panelTabulkyKlientu.ulozPoprveSeznam();
        PanelTabulkyHlidani panelTabulkyHlidani = new PanelTabulkyHlidani();
        panelTabulkyHlidani.ulozPoprveSeznam();
    }

    /**
     * Metoda naplňující grafické prvky daty z databáze při vytvoření scény.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
}
