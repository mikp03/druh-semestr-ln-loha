package ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import logika.Objednavka;
import logika.Osoba;
import logika.ZamestnanecHlidani;
import table.PanelTabulkyHlidani;
import table.PanelTabulkyObjednavek;
import table.PanelTabulkyPoskytovateleHlidani;

import javafx.scene.control.*;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ZalozeniObjednavky implements Initializable {
    @FXML
    private BorderPane borderPane;
    
    @FXML
    private ChoiceBox stavChoice;

    @FXML
    private TextField datumInput;

    @FXML
    private TextField cenaInput;

    @FXML
    private TextField casZacatkuInput;

    @FXML
    private TextField mistoZacatkuInput;

    @FXML
    private TextField poznamkaInput;

    @FXML
    private TextField idObjednavkyInput;

    @FXML
    private TextField jmenoInput;

    private Osoba zamestnanecHlidani;

    /**
     * Metoda, která po zavolání vykreslí scénu s úvodní stránkou.
     *
     * @param event událost při které se má metoda provést
     * @throws Exception
     */
    @FXML
    public void loadUvodniStranka(ActionEvent event) throws Exception {
        BorderPane pane = FXMLLoader.load(getClass().getResource("/fxml/UvodniObrazovka.fxml"));
        borderPane.getChildren().setAll(pane);
    }

    /**
     * Metoda, která po zavolání vykreslí scénu s výpisem všech poskytovatelů hlídání.
     *
     * @param event událost při které se má metoda provést
     * @throws Exception
     */
    @FXML
    public void loadPoskytovateleHlidani(ActionEvent event) throws Exception {
        BorderPane pane = FXMLLoader.load(getClass().getResource("/fxml/SeznamPoskytovateluHlidani.fxml"));
        borderPane.getChildren().setAll(pane);
    }

    /**
     * Metoda, která po zavolání vykreslí scénu s výpisem všech služeb.
     *
     * @param event událost při které se má metoda provést
     * @throws Exception
     */
    @FXML
    public void loadSluzby(ActionEvent event) throws Exception {
        BorderPane pane = FXMLLoader.load(getClass().getResource("/fxml/SeznamSluzeb.fxml"));
        borderPane.getChildren().setAll(pane);
    }

    /**
     * Metoda, která po zavolání vykreslí scénu s výpisem všech objednavek.
     *
     * @param event událost při které se má metoda provést
     * @throws Exception
     */
    @FXML
    public void loadObjednavky(ActionEvent event) throws Exception {
        BorderPane pane = FXMLLoader.load(getClass().getResource("/fxml/SeznamObjednavek.fxml"));
        borderPane.getChildren().setAll(pane);
    }

    /**
     * Metoda, která po zavolání vykreslí scénu s výpisem všech klientů.
     *
     * @param event událost při které se má metoda provést
     * @throws Exception
     */
    @FXML
    public void loadKlienti(ActionEvent event) throws Exception {
        BorderPane pane = FXMLLoader.load(getClass().getResource("/fxml/SeznamKlientu.fxml"));
        borderPane.getChildren().setAll(pane);
    }

    /**
     * Metoda voláná před ukládáním dat do databáze. Kontroluje vstupy, zda
     * splňují podmínky pro uložení, zobrazuje modální okna s informacemi o
     * průběhu ukládání.
     *
     * @param event událost při které se má metoda provést
     */
    @FXML
    public void potvrdit(ActionEvent event) {
//        PanelTabulkyObjednavek panelTabulky = new PanelTabulkyObjednavek();
//        panelTabulky.ulozSeznam();

//        funguje jednou
        int idObjednavkyInteger = Integer.parseInt(idObjednavkyInput.getText());
        int cenaInteger = Integer.parseInt(cenaInput.getText());

        ObservableList<Objednavka> data;
        List<Objednavka> personList;
        data  = FXCollections.observableArrayList();
        personList = new ArrayList<>();

        data  = FXCollections.observableArrayList(new Objednavka(idObjednavkyInteger, jmenoInput.getText(), datumInput.getText(), casZacatkuInput.getText(), mistoZacatkuInput.getText(), cenaInteger, poznamkaInput.getText(), stavChoice.isShowing()));


        for (Objednavka objednavka : data){
            personList.add(objednavka);
        }

        try {
            ObjectOutputStream outp = new ObjectOutputStream(new FileOutputStream( "Objednavky.dat"));
            outp.writeObject(personList);
            data.clear();


            outp.close();

        } catch (Exception e) {
            System.out.println("CHYBA: Nelze vytvorit soubor. " + e.getMessage());
        }
    }

    /**
     * Metoda naplňující grafické prvky daty z databáze při vytvoření scény.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        stavChoice.setItems(FXCollections.observableArrayList(true, false));
    }
}
