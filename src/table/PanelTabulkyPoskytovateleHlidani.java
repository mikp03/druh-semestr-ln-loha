package table;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.FlowPane;
import logika.Klient;
import logika.ZamestnanecHlidani;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class PanelTabulkyPoskytovateleHlidani {
    private FlowPane panelTabulky;
    private TableView<ZamestnanecHlidani> table;
    private ObservableList<ZamestnanecHlidani> data;
    private List<ZamestnanecHlidani> personList;

    public PanelTabulkyPoskytovateleHlidani() {
        panelTabulky = new FlowPane();
        table = new TableView<>();
        table.setEditable(true);
        data  = FXCollections.observableArrayList();
        personList = new ArrayList<>();

        //ulozPoprveSeznam();
        nactiSeznam();

        // String jmeno, String prijmeni, int rokNarozeni, String email, int maxPocetDeti, int hodnoceni


        TableColumn jmenoCol = new TableColumn("Jméno");
        jmenoCol.setMinWidth(100);
        jmenoCol.setCellValueFactory(
                new PropertyValueFactory<>("jmeno"));

        TableColumn prijmeniCol = new TableColumn("Příjmení");
        prijmeniCol.setMinWidth(100);
        prijmeniCol.setCellValueFactory(
                new PropertyValueFactory<>("prijmeni"));

        TableColumn rokNarozeniCol = new TableColumn("Rok narození");
        rokNarozeniCol.setMinWidth(100);
        rokNarozeniCol.setCellValueFactory(
                new PropertyValueFactory<>("rokNarozeni"));

        TableColumn emailCol = new TableColumn("Email");
        emailCol.setMinWidth(200);
        emailCol.setCellValueFactory(
                new PropertyValueFactory<>("email"));

        TableColumn maxPocetDetiCol = new TableColumn("Maximální počet dětí");
        maxPocetDetiCol.setMinWidth(200);
        maxPocetDetiCol.setCellValueFactory(
                new PropertyValueFactory<>("maxPocetDeti"));

        TableColumn hodnoceniCol = new TableColumn("Hodnoceni");
        hodnoceniCol.setMinWidth(200);
        hodnoceniCol.setCellValueFactory(
                new PropertyValueFactory<>("hodnoceni"));

        table.setItems(data);
        table.getColumns().addAll(jmenoCol, prijmeniCol, rokNarozeniCol, emailCol, maxPocetDetiCol, hodnoceniCol);
        panelTabulky.getChildren().add(table);
    }

    public FlowPane getPanelTabulky() {
        return panelTabulky;
    }

    /**
     * prvotní uložení seznamu do souboru.
     */
    public void ulozPoprveSeznam() {
        data  = FXCollections.observableArrayList(
                new ZamestnanecHlidani("Jacob", "Smith", 1980, "jacob.smith@example.com", 2, 8),
                new ZamestnanecHlidani("Isabella", "Johnson", 1990, "isabella.johnson@example.com",2,7),
                new ZamestnanecHlidani("Ethan", "Williams", 1988, "ethan.williams@example.com",4, 8),
                new ZamestnanecHlidani("Emma", "Jones", 1999, "emma.jones@example.com", 3, 9),
                new ZamestnanecHlidani("Michael", "Brown", 2002, "michael.brown@example.com",1,10)
        );

        for (ZamestnanecHlidani zamestnanecHlidani : data){
            personList.add(zamestnanecHlidani);
        }

        try {
            ObjectOutputStream outp = new ObjectOutputStream(new FileOutputStream( "ZamestnanciHlidani.dat"));
            outp.writeObject(personList);


            outp.close();

        } catch (Exception e) {
            System.out.println("CHYBA: Nelze vytvorit soubor. " + e.getMessage());
        }

    }
    /**
     * uloží seznam do souboru.
     */
    public void ulozSeznam() {

        for (ZamestnanecHlidani zamestnanecHlidani : data){
            personList.add(zamestnanecHlidani);
        }
        try {
            ObjectOutputStream outp = new ObjectOutputStream(new FileOutputStream("ZamestnanciHlidani.dat"));
            outp.writeObject(personList);

        } catch (Exception e) {
            System.out.println("CHYBA: Nelze vytvorit soubor. " + e.getMessage());
        }

    }

    /**
     * načte seznam  ze souboru
     */
    public void nactiSeznam() {
        try {
            ObjectInputStream inp = new ObjectInputStream(new FileInputStream("ZamestnanciHlidani.dat"));
            personList = (ArrayList<ZamestnanecHlidani>) inp.readObject();

            for (ZamestnanecHlidani zamestnanecHlidani : personList){
                data.add(zamestnanecHlidani);
            }

        } catch (Exception e) {
            System.out.println("CHYBA: Nelze načíst soubor. " + e.getMessage());
        }


    }
}
