/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.FlowPane;
import logika.Klient;
import logika.ZamestnanecHlidani;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * drží tabulku v pameti vrací tabulku v panelu
 *
 * @author Alena Buchalcevova
 */
public class PanelTabulkyKlientu {

    private  FlowPane panelTabulky;
    private  TableView<Klient> table;
    private  ObservableList<Klient> data;
    private  List<Klient> personList;

    public PanelTabulkyKlientu() {
        panelTabulky = new FlowPane();
        table = new TableView<>();
        table.setEditable(true);
        data  = FXCollections.observableArrayList();
        personList = new ArrayList<>();

        //ulozPoprveSeznam();
        nactiSeznam();

        // String jmeno, String prijmeni, int rokNarozeni, String email, int maxPocetDeti, int hodnoceni


        TableColumn firstNameCol = new TableColumn("Jméno");
        firstNameCol.setMinWidth(100);
        firstNameCol.setCellValueFactory(
                new PropertyValueFactory<>("jmeno"));

        TableColumn lastNameCol = new TableColumn("Příjmení");
        lastNameCol.setMinWidth(100);
        lastNameCol.setCellValueFactory(
                new PropertyValueFactory<>("prijmeni"));

        TableColumn rokNarozeniCol = new TableColumn("Rok narození");
        rokNarozeniCol.setMinWidth(100);
        rokNarozeniCol.setCellValueFactory(
                new PropertyValueFactory<>("rokNarozeni"));

        TableColumn emailCol = new TableColumn("Email");
        emailCol.setMinWidth(200);
        emailCol.setCellValueFactory(
                new PropertyValueFactory<>("email"));

        TableColumn pocetDetiCol = new TableColumn("Počet dětí");
        pocetDetiCol.setMinWidth(200);
        pocetDetiCol.setCellValueFactory(
                new PropertyValueFactory<>("pocetDeti"));

        table.setItems(data);
        table.getColumns().addAll(firstNameCol, lastNameCol, rokNarozeniCol, emailCol, pocetDetiCol);
        panelTabulky.getChildren().add(table);
    }

    public FlowPane getPanelTabulky() {
        return panelTabulky;
    }

    /**
     * prvotní uložení seznamu do souboru.
     */
    public void ulozPoprveSeznam() {
        data  = FXCollections.observableArrayList(
                new Klient("Jacob", "Smith", 1980, "jacob.smith@example.com", 2),
                new Klient("Isabella", "Johnson", 1990, "isabella.johnson@example.com",2),
                new Klient("Ethan", "Williams", 1988, "ethan.williams@example.com",4),
                new Klient("Emma", "Jones", 1999, "emma.jones@example.com", 3),
                new Klient("Michael", "Brown", 2002, "michael.brown@example.com",1)
        );

        for (Klient klient : data){
            personList.add(klient);
        }

        try {
            ObjectOutputStream outp = new ObjectOutputStream(new FileOutputStream( "Klienti.dat"));
            outp.writeObject(personList);


            outp.close();

        } catch (Exception e) {
            System.out.println("CHYBA: Nelze vytvorit soubor. " + e.getMessage());
        }

    }

    /**
     * uloží seznam do souboru.
     */
    public void ulozSeznam(Klient newKlient) {
        data = FXCollections.observableArrayList(newKlient);

        for (Klient klient : data){
            personList.add(klient);
        }

        try {
            ObjectOutputStream outp = new ObjectOutputStream(new FileOutputStream("Klienti.dat"));
            outp.writeObject(personList);

        } catch (Exception e) {
            System.out.println("CHYBA: Nelze vytvorit soubor. " + e.getMessage());
        }

    }

    /**
     * načte seznam  ze souboru
     */
    public void nactiSeznam() {
        try {
            ObjectInputStream inp = new ObjectInputStream(new FileInputStream("Klienti.dat"));
            personList = (ArrayList<Klient>) inp.readObject();

            for (Klient klient : personList){
                data.add(klient);
            }

        } catch (Exception e) {
            System.out.println("CHYBA: Nelze načíst soubor. " + e.getMessage());
        }


    }
}

