package table;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.FlowPane;
import logika.Objednavka;
import logika.ZamestnanecHlidani;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class PanelTabulkyObjednavek {
    private FlowPane panelTabulky;
    private TableView<Objednavka> table;
    private ObservableList<Objednavka> data;
    private List<Objednavka> personList;

    public PanelTabulkyObjednavek() {
        panelTabulky = new FlowPane();
        table = new TableView<>();
        table.setEditable(true);
        data  = FXCollections.observableArrayList();
        personList = new ArrayList<>();

        //ulozPoprveSeznam();
        nactiSeznam();

        // int idObjednavky, String jmenoZamestnanceHlidani, String datum, String casZacatek, String mistoZacatek, int cena, String poznamka, boolean stav


        TableColumn idObjednavkyCol = new TableColumn("ID objednávky");
        idObjednavkyCol.setMinWidth(100);
        idObjednavkyCol.setCellValueFactory(
                new PropertyValueFactory<>("idObjednavky"));

        TableColumn jmenoZamestnanceHlidaniCol = new TableColumn("Jméno zaměstnance hlídání");
        jmenoZamestnanceHlidaniCol.setMinWidth(100);
        jmenoZamestnanceHlidaniCol.setCellValueFactory(
                new PropertyValueFactory<>("jmenoZamestnanceHlidani"));

        TableColumn datumCol = new TableColumn("Datum");
        datumCol.setMinWidth(100);
        datumCol.setCellValueFactory(
                new PropertyValueFactory<>("datum"));

        TableColumn casZacatekCol = new TableColumn("Čas začátku");
        casZacatekCol.setMinWidth(100);
        casZacatekCol.setCellValueFactory(
                new PropertyValueFactory<>("casZacatek"));

        TableColumn mistoZacatekCol = new TableColumn("Místo začátku");
        mistoZacatekCol.setMinWidth(200);
        mistoZacatekCol.setCellValueFactory(
                new PropertyValueFactory<>("mistoZacatek"));

        TableColumn cenaCol = new TableColumn("Cena");
        cenaCol.setMinWidth(100);
        cenaCol.setCellValueFactory(
                new PropertyValueFactory<>("cena"));

        TableColumn poznamkaCol = new TableColumn("Poznámka");
        poznamkaCol.setMinWidth(200);
        poznamkaCol.setCellValueFactory(
                new PropertyValueFactory<>("poznamka"));

        TableColumn stavCol = new TableColumn("Stav");
        stavCol.setMinWidth(100);
        stavCol.setCellValueFactory(
                new PropertyValueFactory<>("stav"));

        table.setItems(data);
        table.getColumns().addAll(idObjednavkyCol, jmenoZamestnanceHlidaniCol, datumCol, casZacatekCol, mistoZacatekCol, cenaCol, poznamkaCol, stavCol);
        panelTabulky.getChildren().add(table);
    }

    public FlowPane getPanelTabulky() {
        return panelTabulky;
    }

    /**
     * prvotní uložení seznamu do souboru.
     */
    public void ulozPoprveSeznam() {
        data  = FXCollections.observableArrayList(
                new Objednavka(01, "Smith", "22. 02. 2020", "10:00", "doma", 300, "Dítě má alergii na arašídy", true)
        );

        for (Objednavka objednavka : data){
            personList.add(objednavka);
        }

        try {
            ObjectOutputStream outp = new ObjectOutputStream(new FileOutputStream( "Objednavky.dat"));
            outp.writeObject(personList);


            outp.close();

        } catch (Exception e) {
            System.out.println("CHYBA: Nelze vytvorit soubor. " + e.getMessage());
        }

    }
    /**
     * uloží seznam do souboru.
     */
    public void ulozSeznam() {

        for (Objednavka objednavka : data){
            personList.add(objednavka);
        }
        try {
            ObjectOutputStream outp = new ObjectOutputStream(new FileOutputStream("Objednavky.dat"));
            outp.writeObject(personList);

        } catch (Exception e) {
            System.out.println("CHYBA: Nelze vytvorit soubor. " + e.getMessage());
        }

    }

    /**
     * načte seznam  ze souboru
     */
    public void nactiSeznam() {
        try {
            ObjectInputStream inp = new ObjectInputStream(new FileInputStream("Objednavky.dat"));
            personList = (ArrayList<Objednavka>) inp.readObject();

            for (Objednavka objednavka : personList){
                data.add(objednavka);
            }

        } catch (Exception e) {
            System.out.println("CHYBA: Nelze načíst soubor. " + e.getMessage());
        }


    }
}
