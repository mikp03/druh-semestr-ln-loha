package table;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.FlowPane;
import logika.Hlidani;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class PanelTabulkyHlidani {
    private FlowPane panelTabulky;
    private TableView<Hlidani> table;
    private ObservableList<Hlidani> data;
    private List<Hlidani> personList;

    public PanelTabulkyHlidani() {
        panelTabulky = new FlowPane();
        table = new TableView<>();
        table.setEditable(true);
        data  = FXCollections.observableArrayList();
        personList = new ArrayList<>();

        //ulozPoprveSeznam();
        nactiSeznam();

        // String jmeno, String prijmeni, int rokNarozeni, String email, int maxPocetDeti, int hodnoceni


        TableColumn typHlidaniCol = new TableColumn("Typ hlídání");
        typHlidaniCol.setMinWidth(100);
        typHlidaniCol.setCellValueFactory(
                new PropertyValueFactory<>("typHlidani"));

        TableColumn delkaHlidaniCol = new TableColumn("Délka hlídání");
        delkaHlidaniCol.setMinWidth(100);
        delkaHlidaniCol.setCellValueFactory(
                new PropertyValueFactory<>("delkaHlidani"));

        TableColumn popisHlidaniCol = new TableColumn("Popis hlídání");
        popisHlidaniCol.setMinWidth(100);
        popisHlidaniCol.setCellValueFactory(
                new PropertyValueFactory<>("popisHlidani"));

        table.setItems(data);
        table.getColumns().addAll(typHlidaniCol, delkaHlidaniCol, popisHlidaniCol);
        panelTabulky.getChildren().add(table);
    }

    public FlowPane getPanelTabulky() {
        return panelTabulky;
    }

    /**
     * prvotní uložení seznamu do souboru.
     */
    public void ulozPoprveSeznam() {
        data  = FXCollections.observableArrayList(
                new Hlidani("denní", "6 hodin", "postarám se o vaše děti"),
                new Hlidani("noční", "10 hodin", "Zatímco jste v práci")
        );

        for (Hlidani hlidani : data){
            personList.add(hlidani);
        }

        try {
            ObjectOutputStream outp = new ObjectOutputStream(new FileOutputStream( "Hlidani.dat"));
            outp.writeObject(personList);


            outp.close();

        } catch (Exception e) {
            System.out.println("CHYBA: Nelze vytvorit soubor. " + e.getMessage());
        }

    }
    /**
     * uloží seznam do souboru.
     */
    public void ulozSeznam() {

        for (Hlidani hlidani : data){
            personList.add(hlidani);
        }
        try {
            ObjectOutputStream outp = new ObjectOutputStream(new FileOutputStream("Hlidani.dat"));
            outp.writeObject(personList);

        } catch (Exception e) {
            System.out.println("CHYBA: Nelze vytvorit soubor. " + e.getMessage());
        }

    }

    /**
     * načte seznam  ze souboru
     */
    public void nactiSeznam() {
        try {
            ObjectInputStream inp = new ObjectInputStream(new FileInputStream("Hlidani.dat"));
            personList = (ArrayList<Hlidani>) inp.readObject();

            for (Hlidani hlidani : personList){
                data.add(hlidani);
            }

        } catch (Exception e) {
            System.out.println("CHYBA: Nelze načíst soubor. " + e.getMessage());
        }


    }

}
