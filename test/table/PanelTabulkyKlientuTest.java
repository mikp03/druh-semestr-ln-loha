package table;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import logika.Hlidani;
import logika.Klient;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class PanelTabulkyKlientuTest {
    private ObservableList<Klient> data;

    @Test
    public void ulozPoprveSeznam() {
        Assert.assertNull(data);
        data  = FXCollections.observableArrayList(
                new Klient("abc", "Abc", 1999, "ABC@abc.cz",3));
        Assert.assertNotNull(data);
    }

}