package table;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import logika.Hlidani;
import logika.Objednavka;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class PanelTabulkyObjednavekTest {
    private ObservableList<Objednavka> data;

    @Test
    public void ulozPoprveSeznam() {
        Assert.assertNull(data);
        data  = FXCollections.observableArrayList(
                new Objednavka(1, "abc", "22. 2. 2020","12:10", "doma - adresa...", 1000, "ibubrufen než přijdete", true));
        Assert.assertNotNull(data);
    }
}