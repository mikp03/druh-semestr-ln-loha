package table;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import logika.Hlidani;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class PanelTabulkyHlidaniTest {

    private ObservableList<Hlidani> data;

    @Test
    public void ulozPoprveSeznam() {
        Assert.assertNull(data);
        data  = FXCollections.observableArrayList(
                new Hlidani("denní", "6 hodin", "postarám se o vaše děti"));
        Assert.assertNotNull(data);
    }
}