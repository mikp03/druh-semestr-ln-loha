package logika;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ZamestnanecHlidaniTest {

    @org.junit.Before
    public void setUp() throws Exception {
    }

    /** Metoda zkoumá, zda-li existuje zaměstnanec a má přiřazení hodnocení
    */
    @Test
            public void existujeZamestnanecHlidani() {
        ZamestnanecHlidani test = new ZamestnanecHlidani("Abel", "Bela", 1980, "Bela.abel@pml.cz", 2, 9);
        test.getHodnoceni();
        Assert.assertEquals(9, test.getHodnoceni());
    }
}